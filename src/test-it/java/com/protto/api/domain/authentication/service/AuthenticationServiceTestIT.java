package com.protto.api.domain.authentication.service;

import com.protto.api.ApplicationIT;
import com.protto.api.domain.authentication.model.dao.AuthenticationRequest;
import com.protto.api.domain.authentication.model.dao.AuthenticationResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;



@Transactional
class AuthenticationServiceTestIT extends ApplicationIT {

    @Autowired
    private AuthenticationService authenticationService;

    @Test
    @Sql("/User.sql")
    void whenLoginSuccessfulInAppConnexion_then_should_returnToken() {
        AuthenticationRequest request = new AuthenticationRequest("dataForTest1@gmail.com", "Tototo77!");

        AuthenticationResponse response = authenticationService.login(request);

        Assertions.assertThat(response.token())
                .isNotNull().isNotBlank();

    }

    @Test
    @Sql("/User.sql")
    void whenLoginWrongPasswordInAppConnexion_then_should_throwException() {
        AuthenticationRequest request = new AuthenticationRequest("dataForTest1@gmail.com", "mauvaispasse");

        Assertions.assertThatThrownBy(()-> authenticationService.login(request))
                .isInstanceOf(BadCredentialsException.class);

    }

    @Test
    @Sql("/User.sql")
    void whenLoginUserNotFoundInAppConnexion_then_should_throwException() {
        AuthenticationRequest request = new AuthenticationRequest("idontexist@gmail.com", "password");

        Assertions.assertThatThrownBy(()-> authenticationService.login(request))
                .isInstanceOf(InternalAuthenticationServiceException.class);
    }

}

