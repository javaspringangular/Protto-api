package com.protto.api.domain.authentication.controller;

import com.protto.api.ApplicationIT;
import com.protto.api.TestUtil;
import com.protto.api.domain.authentication.model.dao.AuthenticationRequest;
import com.protto.api.domain.authentication.model.dao.AuthenticationResponse;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.util.JWTutil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
class AuthenticationControllerTestIT extends ApplicationIT {

    @Autowired
    JWTutil jwTutil;

    @Test
    @Sql("/User.sql")
    void whenLoginSuccessfullInApp_then_should_getTokenAndAccess() throws Exception {

        AuthenticationRequest request = new AuthenticationRequest("dataForTest1@gmail.com", "Tototo77!");

        MvcResult res = mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isOk())
                .andReturn();

        Assertions.assertThat(res.getResponse().getContentAsString())
                .isNotEmpty();
    }

    @Test
    @Sql("/User.sql")
    void whenLoginEmptyUsernameInApp_then_should_throwError() throws Exception {

        AuthenticationRequest authRequest = new AuthenticationRequest("", "wrongpass");

        mockMvc.perform(post("/auth/login")
                        .content(TestUtil.toJSON(authRequest)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Sql("/User.sql")
    void whenLoginSuccessfullInAppAndVerifyIsTokenValide_then_should_getAccess() throws Exception {

        AuthenticationRequest authRequest = new AuthenticationRequest("dataForTest1@gmail.com", "Tototo77!");

        MvcResult res = mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(authRequest)))
                .andExpect(status().isOk())
                .andReturn();

        AuthenticationResponse resp = TestUtil.parseJson(res.getResponse().getContentAsString(),AuthenticationResponse.class).get(0);

        Assertions.assertThat(jwTutil.validateToken(resp.token())).isTrue();
    }

    @Test
    @Sql("/User.sql")
    void whenLoginSuccessfullInAppAndVerifyIsClient_then_should_getClientToken() throws Exception {

        AuthenticationRequest authRequest = new AuthenticationRequest("dataForTest2@gmail.com", "Tototo77!"); //client

        MvcResult res = mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(authRequest)))
                .andExpect(status().isOk())
                .andReturn();

        AuthenticationResponse  resp = TestUtil.parseJson(res.getResponse().getContentAsString(),AuthenticationResponse.class).get(0);

        Assertions.assertThat(jwTutil.getAuthoritiesFromToken(resp.token()))
                .contains(new SimpleGrantedAuthority(ERole.CLIENT.name()));
    }

    @Test
    @Sql("/User.sql")
    void whenLoginSuccessfullInAppAndVerifyIsAdmin_then_should_getAdminToken() throws Exception {

        AuthenticationRequest authRequest = new AuthenticationRequest("dataForTest1@gmail.com", "Tototo77!"); //admin

        MvcResult res = mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(authRequest)))
                .andExpect(status().isOk())
                .andReturn();

        AuthenticationResponse  resp = TestUtil.parseJson(res.getResponse().getContentAsString(),AuthenticationResponse.class).get(0);

        Assertions.assertThat(jwTutil.getAuthoritiesFromToken(resp.token()))
                .contains(new SimpleGrantedAuthority(ERole.ADMIN.name()));
    }


}
