package com.protto.api.domain.user.controller;

import com.protto.api.ApplicationIT;
import com.protto.api.TestUtil;
import com.protto.api.domain.user.model.dao.UserCreateRequest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
class UserControllerTestIT extends ApplicationIT {

    @Test
    @Sql("/Repository.sql")
    void whenCreateUser_then_ShouldSuccess() throws Exception {
        UserCreateRequest userToCreate = new UserCreateRequest(
                "User4",
                "Test4",
                "dataForTest4@consultant-solutec.fr",
                "Tototo77!");

        MvcResult res = mockMvc.perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(userToCreate)))
                .andExpect(status().isCreated())
                .andReturn();

        Assertions.assertThat(res.getResponse().getContentAsString()).isEqualTo("4");
    }

    @Test
    @Sql("/Repository.sql")
    void whenCreateUser_then_shouldThrowErrorNullFirstName() throws Exception {
        UserCreateRequest userToCreate = new UserCreateRequest(
                "",
                "Test4",
                "dataForTest4@gmail.com",
                "Tototo77!");

        mockMvc.perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(userToCreate)))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    @Sql("/Repository.sql")
    void whenCreateUser_then_shouldThrowErrorNullLastName() throws Exception {
        UserCreateRequest userToCreate = new UserCreateRequest(
                "test4",
                "",
                "dataForTest4@gmail.com",
                "Tototo77!");

        mockMvc.perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(userToCreate)))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }
    @Test
    @Sql("/Repository.sql")
    void whenCreateUser_then_shouldThrowErrorNullLogin() throws Exception {
        UserCreateRequest userToCreate = new UserCreateRequest(
                "test4",
                "Test4",
                "",
                "Tototo77!");

        mockMvc.perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(userToCreate)))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    @Sql("/Repository.sql")
    void whenCreateUser_then_shouldThrowErrorNullPassword() throws Exception {
        UserCreateRequest userToCreate = new UserCreateRequest(
                "test4",
                "Test4",
                "dataForTest4@gmail.com",
                "");

        mockMvc.perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(userToCreate)))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    @Sql("/Repository.sql")
    void whenCreateUser_then_shouldThrowErrorAlreadyRegistered() throws Exception {
        UserCreateRequest userToCreate = new UserCreateRequest(
                "test4",
                "Test4",
                "dataForTest1@gmail.com", //deja enregistré
                "Tototo77!");

        mockMvc.perform(post("/user/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(userToCreate)))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    @Sql("/Repository.sql")
    void whenFindUserByIdString_then_shouldThrowError() throws Exception {
        mockMvc.perform(get("/user/toto"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql("/Repository.sql")
    void whenFindUserByIdNull_then_shouldThrowError() throws Exception {
        mockMvc.perform(get("/user/"+null))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql("/Repository.sql")
    @WithMockUser(username = "toto", authorities = {"CLIENT"})
    void whenFindUserByIdAsClient_then_shouldThrowError() {
        Assertions.assertThatThrownBy(() ->
            mockMvc.perform(get("/user/1"))
                    .andExpect(status().isForbidden())
        ).hasRootCauseInstanceOf(AccessDeniedException.class);
    }

    @Test
    @Sql("/Repository.sql")
    @WithMockUser(username = "toto", authorities = {"CLIENT"})
    void whenGetAllUserAsClient_then_shouldThrowError() {
        Assertions.assertThatThrownBy(() ->
                mockMvc.perform(get("/user"))
                        .andExpect(status().isForbidden())
        ).hasRootCauseInstanceOf(AccessDeniedException.class);
    }
}
