package com.protto.api.domain.user.controller;

import com.protto.api.conf.AbstractSecurityConfigTest;
import com.protto.api.domain.user.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerSecurityTestIT extends AbstractSecurityConfigTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    @WithMockUser(username = "willy", authorities = {"CLIENT"})
    void whenGetAllUserAsClient_then_should_beForbidden() throws Exception {
        mockMvc.perform(get("/user"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "toto", authorities = {"ADMIN"})
    void whenGetAllUserAsAdmin_then_should_beOk() throws Exception {
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "toto", authorities = {"ADMIN"})
    void whenFindUserByIdWithAdminAccess_then_should_beOk() throws Exception {
        mockMvc.perform(get("/user/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "willy", authorities = {"CLIENT"})
    void whenFindUserByIdAsClient_then_shouldForbidden() throws Exception {
        mockMvc.perform(get("/user/1"))
                        .andExpect(status().isForbidden());
    }






}
