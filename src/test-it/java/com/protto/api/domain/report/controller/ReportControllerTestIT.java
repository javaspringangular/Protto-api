package com.protto.api.domain.report.controller;

import com.protto.api.ApplicationIT;

import com.protto.api.domain.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ReportControllerTestIT extends ApplicationIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReportService reportService;

    /*
    @Test
    @Sql("/Repository.sql")
    @WithMockUser(username = "willy",  authorities = {("CLIENT")})
    void whenCreateReport_then_should_success() throws Exception {
        ReportCreateRequest request = new ReportCreateRequest(
                LocalDate.now(),
                "comExchanges",
                "selfStudies",
                "projects"
        );

        MvcResult result = mockMvc.perform(post("/report/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isCreated())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("3");
    } */
}
