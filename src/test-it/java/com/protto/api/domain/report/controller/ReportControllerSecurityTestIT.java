package com.protto.api.domain.report.controller;

import com.protto.api.TestUtil;
import com.protto.api.conf.AbstractSecurityConfigTest;
import com.protto.api.domain.report.model.dao.ReportCreateRequest;
import com.protto.api.domain.report.model.dao.ReportDateRequest;
import com.protto.api.domain.report.service.ReportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReportController.class)
public class ReportControllerSecurityTestIT extends AbstractSecurityConfigTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReportService reportService;
    
    @Test
    @WithMockUser(username = "toto", authorities = {"ADMIN"})
    void whenCreateReport_then_should_beForbidden() throws Exception {
        ReportCreateRequest request = new ReportCreateRequest(
                LocalDate.now(),
                "com",
                "self",
                "project"
        );

        mockMvc.perform(post("/report/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.toJSON(request)))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "willy", authorities = {"CLIENT"})
    void whenCreateReport_then_should_beOk() throws Exception {
        ReportCreateRequest request = new ReportCreateRequest(
                LocalDate.now(),
                "com",
                "self",
                "project"
        );

        mockMvc.perform(post("/report/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = "willy", authorities = {"CLIENT"})
    void whenGetAllReports_then_should_beForbidden() throws Exception {
        mockMvc.perform(get("/report")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "toto", authorities = {"ADMIN"})
    void whenGetAllReports_then_should_beOk() throws Exception {

        mockMvc.perform(get("/report")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "toto", authorities = {"ADMIN"})
    void whenGetMyReports_then_should_beForbidden() throws Exception {
        mockMvc.perform(get("/report/myReports")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "willy", authorities = {"CLIENT"})
    void whenGetMyReports_then_should_beOk() throws Exception {
        mockMvc.perform(get("/report/myReports")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "toto", authorities = {"ADMIN"})
    void whenGetMyReportsFilterByDate_then_should_beForbidden() throws Exception {
        mockMvc.perform(get("/report/myReports")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "toto", authorities = {"CLIENT"})
    void whenGetMyReportsFilterByDate_then_should_beOk() throws Exception {
        ReportDateRequest request = new ReportDateRequest(2023, 2);

        mockMvc.perform(post("/report/myReports/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isOk());
    }

}
