-----INSERT USER-----

INSERT INTO t_user (user_id, user_first_name, user_last_name, user_login, user_password, user_role) VALUES (1L, 'Ottman', 'Becuwe', 'dataForTest1@gmail.com', '$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O', 'ADMIN');
INSERT INTO t_user (user_id,user_first_name, user_last_name, user_login, user_password, user_role) VALUES (2L, 'William', 'Becuwe', 'dataForTest2@gmail.com', '$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O', 'CLIENT');
INSERT INTO t_user (user_id, user_first_name, user_last_name, user_login, user_password, user_role) VALUES (3L, 'Alexandre', 'Becuwe', 'dataForTest3@gmail.com', '$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O', 'CLIENT');

-----INSERT REPORT-----

INSERT INTO t_weekly_report (report_id, report_name, report_date, report_deposit_date, report_user_login, report_com_exchanges, report_self_studies, report_projects) VALUES (1L, 'Weekly_report_willy_20230106', '2023-01-06', '2023-01-06', 'dataForTest2@gmail.com', 'Content1', 'Content2', 'Content3');
INSERT INTO t_weekly_report (report_id, report_name, report_date, report_deposit_date, report_user_login, report_com_exchanges, report_self_studies, report_projects) VALUES (2L, 'Weekly_report_willy_20230113', '2023-01-13', '2023-01-13', 'dataForTest2@gmail.com', 'Content1', 'Content2', 'Content3');
