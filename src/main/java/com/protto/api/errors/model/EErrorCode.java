package com.protto.api.errors.model;

public enum EErrorCode {
    LOGIN_BAD_CREDENTIAL("login.bad.credential"),
    FILE_ALREADY_EXISTS("file.already.exists");

    public final String value;

    EErrorCode(String value){
        this.value = value;
    }
}
