package com.protto.api.errors.exceptions;

import com.protto.api.errors.model.EErrorCode;
import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.nio.file.FileAlreadyExistsException;

@ControllerAdvice
public class GlobalExceptionHandler extends AbstractExceptionHandler {

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorResponse> customHandleBadCredential(Exception ex) {

        ErrorResponse errors = new ErrorResponse();
        errors.setStatus(HttpStatus.FORBIDDEN);
        errors.setMessage(ex.getMessage());
        errors.setErrorCode(EErrorCode.LOGIN_BAD_CREDENTIAL.value);

        return new ResponseEntity<>(errors, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(FileAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> customHandlerFileAlreadyExists(Exception ex) {
        ErrorResponse errors = new ErrorResponse();
        errors.setStatus(HttpStatus.NOT_ACCEPTABLE);
        errors.setMessage(ex.getMessage());
        errors.setErrorCode(EErrorCode.FILE_ALREADY_EXISTS.value);

        return new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
    }
}
