package com.protto.api.errors.exceptions;

import com.protto.api.errors.model.ErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AbstractErrorRuntimeException extends RuntimeException  {
    private final ErrorResponse errorResponse;
    protected AbstractErrorRuntimeException(String message, HttpStatus httpStatus, String errorCode) {
        super(message);
        this.errorResponse = new ErrorResponse(httpStatus, message, errorCode);
    }
}
