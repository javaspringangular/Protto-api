package com.protto.api.errors.exceptions;

import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.ResponseEntity;

public abstract class AbstractExceptionHandler {

    protected ResponseEntity<ErrorResponse> handleException(AbstractErrorRuntimeException ex) {
        return new ResponseEntity<>(ex.getErrorResponse(), ex.getErrorResponse().getStatus());
    }
}
