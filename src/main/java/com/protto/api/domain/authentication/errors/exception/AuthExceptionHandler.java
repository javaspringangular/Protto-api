package com.protto.api.domain.authentication.errors.exception;

import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import com.protto.api.errors.exceptions.AbstractExceptionHandler;
import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AuthExceptionHandler extends AbstractExceptionHandler {

    @ExceptionHandler({
            SamePasswordException.class,
    })
    public ResponseEntity<ErrorResponse> customHandlerSamePassword(AbstractErrorRuntimeException exception) {
        return handleException(exception);
    }
}
