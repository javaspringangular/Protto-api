package com.protto.api.domain.authentication.model.enums;

public enum ERole {
    ADMIN,
    CLIENT
}