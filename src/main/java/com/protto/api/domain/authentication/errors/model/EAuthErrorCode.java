package com.protto.api.domain.authentication.errors.model;

public enum EAuthErrorCode {
        PASSWORD_MUST_NOT_MATCH("password.must.not.match");

        public final String value;

        EAuthErrorCode(String value){
            this.value = value;
        }
}
