package com.protto.api.domain.authentication.model.dao;

import javax.validation.constraints.NotEmpty;

public record ResetPasswordRequest(
        @NotEmpty String login,
        @NotEmpty String phoneNumber
) {
}
