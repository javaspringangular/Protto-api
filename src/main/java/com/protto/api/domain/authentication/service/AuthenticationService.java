package com.protto.api.domain.authentication.service;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.data.repositories.UserRepository;
import com.protto.api.domain.authentication.model.dao.AuthenticationRequest;
import com.protto.api.domain.authentication.model.dao.AuthenticationResponse;
import com.protto.api.domain.authentication.model.dao.ResetPasswordRequest;
import com.protto.api.domain.user.errors.exceptions.UserNotFoundException;
import com.protto.api.util.JWTutil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final JWTutil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    public AuthenticationResponse login(AuthenticationRequest request) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                request.username(), request.password()));

        UserDetails logged = (UserDetails) authentication.getPrincipal();

        return new AuthenticationResponse(jwtUtil.generateToken(logged));
    }

    public void resetPassword(ResetPasswordRequest request) {
        UserEntity user = userRepository.findByEmail(request.login()).orElseThrow(
                () -> new UserNotFoundException(request.login()));


        //"User: %s %s is asking for a new password",  user.getFirstName(),user.getLastName()
        //"Phone number: %s" , request.phoneNumber()
        //"Let's implement a mailing service"
    }

}
