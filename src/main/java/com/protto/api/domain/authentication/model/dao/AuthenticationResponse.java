package com.protto.api.domain.authentication.model.dao;

public record AuthenticationResponse(
        String token) {
}
