package com.protto.api.domain.authentication.errors.exception;

import com.protto.api.domain.authentication.errors.model.EAuthErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class SamePasswordException extends AbstractErrorRuntimeException {

    public SamePasswordException() {
        super(
                "Les deux mots de passe doivent être différents",
                HttpStatus.BAD_REQUEST,
                EAuthErrorCode.PASSWORD_MUST_NOT_MATCH.value
        );
    }
}
