package com.protto.api.domain.authentication.model.dao;

import javax.validation.constraints.NotEmpty;

public record AuthenticationRequest(
        @NotEmpty String username,
        @NotEmpty String password) {
}
