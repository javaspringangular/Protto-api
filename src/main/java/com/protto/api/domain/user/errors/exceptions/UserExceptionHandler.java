package com.protto.api.domain.user.errors.exceptions;

import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import com.protto.api.errors.exceptions.AbstractExceptionHandler;
import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionHandler extends AbstractExceptionHandler {

    @ExceptionHandler({
            UserNotFoundException.class,
            UserAlreadyExistException.class,
            UserLoginPatternException.class
    })
    public ResponseEntity<ErrorResponse> customHandlerUser(AbstractErrorRuntimeException exception) {
        return handleException(exception);
    }


}
