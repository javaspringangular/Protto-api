package com.protto.api.domain.user.model.dao;

import com.protto.api.domain.authentication.model.enums.ERole;

public record UserVO(Long id,
                     String firstName,
                     String lastName,
                     String login,
                     ERole role) {
}
