package com.protto.api.domain.user.errors.model;

public enum EUserErrorCode {
    USER_NOT_FOUND("user.not.found"),
    USER_ARLEADY_EXIST("user.already.exist"),
    USER_LOGIN_PATTERN("user.login.pattern");

    public final String value;

    EUserErrorCode(String value){
        this.value = value;
    }
}
