package com.protto.api.domain.user.model.mapper;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.domain.user.model.dao.UserInfo;
import com.protto.api.domain.user.model.dao.UserVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface UserMapper {

    UserVO entityToUserVO(UserEntity userEntity);

    @Mapping(source = "userEntity.firstName" , target = "firstName")
    @Mapping(source = "userEntity.lastName" , target = "lastName")
    UserInfo entityToUserInfo(UserEntity userEntity);

}
