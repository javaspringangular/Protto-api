package com.protto.api.domain.user.model.dao;

public record UserInfo(String firstName,
                       String lastName) {
}
