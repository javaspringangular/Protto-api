package com.protto.api.domain.user.service;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.domain.user.errors.exceptions.UserLoginPatternException;
import com.protto.api.domain.user.errors.exceptions.UserNotFoundException;
import com.protto.api.domain.user.errors.exceptions.UserAlreadyExistException;
import com.protto.api.domain.user.model.dao.UserCreateRequest;

import com.protto.api.data.repositories.UserRepository;
import com.protto.api.domain.user.model.dao.UserInfo;
import com.protto.api.domain.user.model.dao.UserVO;
import com.protto.api.domain.user.model.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final BCryptPasswordEncoder encoder;

    @Transactional(propagation = Propagation.REQUIRED)
    public Long create(UserCreateRequest userCreateRequest) {
        if(isAlreadyRegistered(userCreateRequest.login())) {
            throw new UserAlreadyExistException(userCreateRequest.login());
        }
        UserEntity user = new UserEntity();

        user.setFirstName(userCreateRequest.firstName());
        user.setLastName(userCreateRequest.lastName());
        user.setLogin(userCreateRequest.login());
        user.setPassword(encoder.encode(userCreateRequest.password()));
        user.setRole(getRoleFromLogin(userCreateRequest.login()));
        return userRepository.save(user).getId();
    }
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<UserVO> getAllUsers() {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(userMapper::entityToUserVO)
                .toList();
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public UserVO findById(Long id) {
        return userRepository.findById(id)
                .map(userMapper::entityToUserVO)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public boolean isAlreadyRegistered(String email) {
        return userRepository.findByEmail(email).isPresent();
    }


    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public Optional<UserEntity> getUserByUserName(String username) {
        return userRepository.findByEmail(username);
    }

    ERole getRoleFromLogin(String login) {
        if(login.contains("@consultant-solutec.fr"))
            return ERole.CLIENT;
        if(login.contains("@solutec.fr"))
            return ERole.ADMIN;
        throw new UserLoginPatternException(login);
    }

    public boolean isAdmin(Authentication loggedUser) {
        return loggedUser.getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(ERole.ADMIN.name()));
    }

    public boolean isClient(Authentication loggedUser) {
        return loggedUser.getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(ERole.CLIENT.name()));
    }

    public UserEntity getUserByAuthentication(Authentication loggedUser) {
        UserDetails userDetails = (UserDetails) loggedUser.getPrincipal();

        return userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(() -> new UserNotFoundException(userDetails.getUsername()));
    }
    public UserInfo getUserInfoByAuthentication(Authentication loggedUser){
        UserDetails userDetails = (UserDetails) loggedUser.getPrincipal();

        return userRepository.findByEmail(userDetails.getUsername()).map(userMapper::entityToUserInfo)
                .orElseThrow(() -> new UserNotFoundException(userDetails.getUsername()));
    }

}
