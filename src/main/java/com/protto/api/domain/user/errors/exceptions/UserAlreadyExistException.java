package com.protto.api.domain.user.errors.exceptions;

import com.protto.api.domain.user.errors.model.EUserErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class UserAlreadyExistException extends AbstractErrorRuntimeException {

    public UserAlreadyExistException(String name) {
        super(
                String.format("L'utilisateur %s existe déjà", name),
                HttpStatus.BAD_REQUEST,
                EUserErrorCode.USER_ARLEADY_EXIST.value
        );
    }
}
