package com.protto.api.domain.user.errors.exceptions;

import com.protto.api.domain.user.errors.model.EUserErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class UserNotFoundException extends AbstractErrorRuntimeException {

    public UserNotFoundException(Long id) {
        super(
                String.format("User: %d Not found", id),
                HttpStatus.NOT_FOUND,
                EUserErrorCode.USER_NOT_FOUND.value);
    }

    public UserNotFoundException(String name) {
        super(
                String.format("User: %s Not found", name),
                HttpStatus.NOT_FOUND,
                EUserErrorCode.USER_NOT_FOUND.value);
    }
}
