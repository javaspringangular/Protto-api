package com.protto.api.domain.user.errors.exceptions;

import com.protto.api.domain.user.errors.model.EUserErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class UserLoginPatternException extends AbstractErrorRuntimeException {

    public UserLoginPatternException(String login) {
        super(
                String.format("Le pattern d'email n'est pas respecté, erreur: %s ", login),
                HttpStatus.BAD_REQUEST,
                EUserErrorCode.USER_LOGIN_PATTERN.value
        );
    }
}