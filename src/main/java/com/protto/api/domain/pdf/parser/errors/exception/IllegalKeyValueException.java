package com.protto.api.domain.pdf.parser.errors.exception;

import com.protto.api.domain.pdf.parser.errors.model.EParserErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class IllegalKeyValueException extends AbstractErrorRuntimeException {

    public IllegalKeyValueException(String key) {
        super(
                "Key value is not correct: " + key,
                HttpStatus.BAD_REQUEST,
                EParserErrorCode.PARSER_ERROR_RANGE_KEY.value
        );
    }
}
