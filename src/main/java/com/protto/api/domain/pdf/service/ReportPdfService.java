package com.protto.api.domain.pdf.service;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.domain.pdf.errors.exception.ErrorUploadingFileException;
import com.protto.api.domain.pdf.util.PdfTemplateUtil;
import com.protto.api.domain.report.errors.exceptions.ReportNoAccessException;
import com.protto.api.domain.report.model.dao.ReportVO;
import com.protto.api.domain.report.service.ReportService;
import com.protto.api.domain.user.errors.exceptions.UserNotFoundException;
import com.protto.api.domain.report.errors.exceptions.NoMatchBetweenUserLoggedAndUserInReportException;
import com.protto.api.domain.pdf.errors.exception.FormatDateInReportException;
import com.protto.api.domain.user.model.dao.UserVO;
import com.protto.api.domain.user.model.mapper.UserMapper;
import com.protto.api.domain.user.service.UserService;
import com.protto.api.util.FileUtil;
import com.protto.api.util.PdfUtil;

import lombok.RequiredArgsConstructor;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReportPdfService {

    private final TemplateEngine templateEngine;
    private final UserService userService;
    private final ReportService reportService;
    private final UserMapper userMapper;

    public void uploadReport(MultipartFile file, Authentication loggedUser) throws FileAlreadyExistsException {
        String fileName = file.getOriginalFilename();
        try {
            FileUtil.copy(file, fileName);
            Map<String, String> request = FileUtil.parseFile(fileName);
            FileUtil.deleteFile(fileName);

            reportService.create(request, loggedUser);
        }
        catch(IOException e) {
            throw new ErrorUploadingFileException("Error uploading file");
        }catch (NoMatchBetweenUserLoggedAndUserInReportException e) {
            throw new NoMatchBetweenUserLoggedAndUserInReportException("Error matching userLogged and userInReport");
        } catch (FormatDateInReportException e) {
            throw new FormatDateInReportException("Date format illegal, should be YYYY-MM-DD");
        }
        catch (Exception e) {
            throw new FileAlreadyExistsException("A file of that name already exists.");
        }
    }

    public byte[] generateReportPdf(Long id, Authentication loggedUser) {
        if(!reportService.hasAccessP(id, loggedUser))
            throw new ReportNoAccessException(id);

        Map<String, Object> data = new HashMap<>();
        ReportVO report = reportService.getReport(id);

        Optional<UserEntity> user = userService.getUserByUserName(report.userLogin());

        if(user.isEmpty())
            throw new UserNotFoundException(report.userLogin());

        UserVO userVO = userMapper.entityToUserVO(user.get());

        data.put("user", userVO);
        data.put("report", report);
        Context context = new Context();
        context.setVariables(data);

        String htmlContent = templateEngine.process(
                PdfTemplateUtil.THYMELEAF_REPORT_HTML,
                context
        );

        return PdfUtil.generatePdfFromHtmlContent(htmlContent);
    }

    public byte[] getReportTemplate() {
        Context context = new Context();

        String htmlContent = templateEngine.process(
                PdfTemplateUtil.THYMELEAF_REPORT_TEMPLATE_HTML,
                context
        );

        return PdfUtil.generatePdfFromHtmlContent(htmlContent);
    }
}
