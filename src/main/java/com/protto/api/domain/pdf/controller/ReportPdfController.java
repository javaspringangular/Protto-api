package com.protto.api.domain.pdf.controller;

import com.protto.api.domain.pdf.service.ReportPdfService;
import com.protto.api.domain.report.service.ReportService;
import com.protto.api.util.FileUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.nio.file.FileAlreadyExistsException;
@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping("/report")
public class ReportPdfController {

    private final ReportPdfService reportPdfService;
    private final ReportService reportService;

    @PostMapping("/uploadReport")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT)")
    public ResponseEntity<Void> uploadReport(@RequestParam("file") MultipartFile file, Authentication loggedUser) throws FileAlreadyExistsException {
        reportPdfService.uploadReport(file, loggedUser);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/pdf/{id}")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT, " +
            " T(com.protto.api.domain.authentication.model.enums.ERole).ADMIN)")
    public ResponseEntity<byte[]> generateQuotationPdf(@PathVariable @NotNull @Min(1) Long id, Authentication loggedUser) {
        byte[] file = reportPdfService.generateReportPdf(id, loggedUser);
        String fileName = reportService.getNameFromReport(id);
        HttpHeaders headers = FileUtil.generateHeaders(file, fileName, MediaType.APPLICATION_PDF_VALUE);
        return new ResponseEntity<>(file, headers , HttpStatus.OK);
    }

    @GetMapping("/template")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT, " +
            " T(com.protto.api.domain.authentication.model.enums.ERole).ADMIN)")
    public ResponseEntity<byte[]> getReportTemplate() {
        byte[] file = reportPdfService.getReportTemplate();
        HttpHeaders headers = FileUtil.generateHeaders(file,"reportTemplate.pdf", MediaType.APPLICATION_PDF_VALUE);
        return new ResponseEntity<>(file, headers , HttpStatus.OK);
    }


}
