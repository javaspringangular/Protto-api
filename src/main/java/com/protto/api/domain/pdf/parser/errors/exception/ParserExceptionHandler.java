package com.protto.api.domain.pdf.parser.errors.exception;

import com.protto.api.domain.pdf.errors.exception.ErrorUploadingFileException;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import com.protto.api.errors.exceptions.AbstractExceptionHandler;
import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class ParserExceptionHandler extends AbstractExceptionHandler {

    @ExceptionHandler({
            IllegalDataInDocumentException.class,
            IllegalKeyValueException.class
    })
    public ResponseEntity<ErrorResponse> customParserHandler(AbstractErrorRuntimeException exception) {
        return handleException(exception);
    }
}