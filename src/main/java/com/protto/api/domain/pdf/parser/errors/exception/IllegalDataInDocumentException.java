package com.protto.api.domain.pdf.parser.errors.exception;

import com.protto.api.domain.pdf.parser.errors.model.EParserErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class IllegalDataInDocumentException extends AbstractErrorRuntimeException {

    public IllegalDataInDocumentException() {
        super(
                "Data from template are broken",
                HttpStatus.BAD_REQUEST,
                EParserErrorCode.PARSER_ERROR_DATA_FROM_TEMPLATE.value
        );
    }
}