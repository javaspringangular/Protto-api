package com.protto.api.domain.pdf.parser.service;

import com.protto.api.util.model.ModelParser;
import com.protto.api.domain.pdf.parser.errors.exception.IllegalDataInDocumentException;
import com.protto.api.domain.pdf.parser.errors.exception.IllegalKeyValueException;
import com.protto.api.util.model.Range;

import java.util.Map;

public class Parser {

    private Parser(){
        throw new IllegalStateException("Utility class");
    }

    public static Map<String, String> parse(String document) {
        Map<String, String> map = new java.util.HashMap<>(Map.of(
                ModelParser.EMAIL_KEY, "",
                ModelParser.REPORT_DATE_KEY, "",
                ModelParser.LASTNAME_KEY, "",
                ModelParser.FIRSTNAME_KEY, "",
                ModelParser.COM_KEY, "",
                ModelParser.SELF_KEY, "",
                ModelParser.PROJECT_KEY, ""));

        for(Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            Range range = getRange(key, document);
            StringBuilder stringBuilder = new StringBuilder();

            for(int i = range.getStartRange(); i < range.getEndRange(); i++) {
                stringBuilder.append(document.charAt(i));
            }

            if(stringBuilder.toString().isEmpty())
                throw new IllegalDataInDocumentException();

            map.put(key, stringBuilder.toString().trim());
        }

        return map;
    }

    public static Range getRange(String key, String document) {
        int start = document.indexOf(key) + key.length();

        switch (key) {
            case ModelParser.EMAIL_KEY -> {
                return new Range(start, document.indexOf(ModelParser.REPORT_DATE_KEY));
            }
            case ModelParser.REPORT_DATE_KEY -> {
                return new Range(start, document.indexOf(ModelParser.LASTNAME_KEY));
            }
            case ModelParser.LASTNAME_KEY -> {
                return new Range(start, document.indexOf(ModelParser.FIRSTNAME_KEY));
            }
            case ModelParser.FIRSTNAME_KEY -> {
                return new Range(start, document.indexOf(ModelParser.TITLE_KEY));
            }
            case ModelParser.COM_KEY -> {
                return new Range(start, document.indexOf(ModelParser.SELF_KEY));
            }
            case ModelParser.SELF_KEY -> {
                return new Range(start, document.indexOf(ModelParser.PROJECT_KEY));
            }
            case ModelParser.PROJECT_KEY -> {
                return new Range(start, document.indexOf(ModelParser.EOF));
            }
            default -> throw new IllegalKeyValueException(key);
        }
    }

}
