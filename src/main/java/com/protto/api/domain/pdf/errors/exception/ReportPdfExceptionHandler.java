package com.protto.api.domain.pdf.errors.exception;

import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import com.protto.api.errors.exceptions.AbstractExceptionHandler;
import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ReportPdfExceptionHandler extends AbstractExceptionHandler {

    @ExceptionHandler({
            ErrorUploadingFileException.class,
            ErrorGeneratingPdfFileException.class,
            FormatDateInReportException.class
    })
    public ResponseEntity<ErrorResponse> customHandlerReportPdf(AbstractErrorRuntimeException exception) {
        return handleException(exception);
    }
}