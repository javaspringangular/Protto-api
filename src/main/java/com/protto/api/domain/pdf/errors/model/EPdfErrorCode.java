package com.protto.api.domain.pdf.errors.model;

public enum EPdfErrorCode {

    REPORT_UPLOADING_ERROR("report.uploading.error"),
    PDF_GENERATING_ERROR("pdf.generating.error"),
    PDF_UPLOADING_ERROR_DATE_FORMAT("pdf.uploading.error.date.format");

    public final String value;

    EPdfErrorCode(String value){
        this.value = value;
    }

}
