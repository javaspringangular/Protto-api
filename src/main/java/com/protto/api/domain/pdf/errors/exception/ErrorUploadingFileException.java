package com.protto.api.domain.pdf.errors.exception;

import com.protto.api.domain.pdf.errors.model.EPdfErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class ErrorUploadingFileException extends AbstractErrorRuntimeException {
    public ErrorUploadingFileException(String message) {
        super(
                message,
                HttpStatus.BAD_REQUEST,
                EPdfErrorCode.REPORT_UPLOADING_ERROR.value
        );
    }

}
