package com.protto.api.domain.pdf.errors.exception;

import com.protto.api.domain.pdf.errors.model.EPdfErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class FormatDateInReportException extends AbstractErrorRuntimeException {
    public FormatDateInReportException(String message) {
        super(
                message,
                HttpStatus.BAD_REQUEST,
                EPdfErrorCode.PDF_UPLOADING_ERROR_DATE_FORMAT.value
        );
    }
}
