package com.protto.api.domain.pdf.parser.errors.model;

public enum EParserErrorCode {


    PARSER_ERROR_RANGE_KEY("parser.error.range"),
    PARSER_ERROR_DATA_FROM_TEMPLATE("parser.error.data");

    public final String value;

    EParserErrorCode(String value){
        this.value = value;
    }
}
