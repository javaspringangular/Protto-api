package com.protto.api.domain.pdf.errors.exception;

import com.protto.api.domain.pdf.errors.model.EPdfErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class ErrorGeneratingPdfFileException extends AbstractErrorRuntimeException {
    public ErrorGeneratingPdfFileException() {
        super(
                "Erreur durant la génération du pdf",
                HttpStatus.BAD_REQUEST,
                EPdfErrorCode.PDF_GENERATING_ERROR.value
        );
    }
}
