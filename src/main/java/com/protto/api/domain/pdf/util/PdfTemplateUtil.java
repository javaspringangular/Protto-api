package com.protto.api.domain.pdf.util;

public class PdfTemplateUtil {

    public static final String THYMELEAF_REPORT_HTML = "report.html"; //rapport a generer lors d'une demande client

    public static final String THYMELEAF_REPORT_TEMPLATE_HTML = "reportTemplate.html"; //template a télécharger puis a completer depuis la page home
}
