package com.protto.api.domain.report.model.dao;

import java.time.LocalDate;

public record ReportVO(
        Long id,
        String name,
        LocalDate reportDate,
        LocalDate depositDate,
        String userLogin,
        String comExchanges,
        String selfStudies,
        String projects
) {
}
