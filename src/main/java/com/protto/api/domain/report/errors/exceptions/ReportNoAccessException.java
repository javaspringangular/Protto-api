package com.protto.api.domain.report.errors.exceptions;

import com.protto.api.domain.report.errors.model.EReportErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class ReportNoAccessException extends AbstractErrorRuntimeException {

    public ReportNoAccessException(Long id) {
        super(
                "No access to report : " + id,
                HttpStatus.FORBIDDEN,
                EReportErrorCode.REPORT_NO_ACCESS.value
        );
    }
}
