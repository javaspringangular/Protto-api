package com.protto.api.domain.report.errors.exceptions;

import com.protto.api.domain.report.errors.model.EReportErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class NoMatchBetweenUserLoggedAndUserInReportException extends AbstractErrorRuntimeException {
    public NoMatchBetweenUserLoggedAndUserInReportException(String userLogin) {
        super(
                "Génération du rapport impossible, user :" + userLogin,
                HttpStatus.BAD_REQUEST,
                EReportErrorCode.PDF_GENERATING_ERROR_USER_MATCH.value);
    }
}
