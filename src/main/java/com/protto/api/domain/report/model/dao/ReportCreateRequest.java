package com.protto.api.domain.report.model.dao;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public record ReportCreateRequest(
        @NotNull LocalDate reportDate,
        @NotEmpty @Length(min = 1, max = 4000) String comExchanges,
        @NotEmpty @Length(min = 1, max = 4000) String selfStudies,
        @NotEmpty @Length(min = 1, max = 4000) String projects
        ) {
}
