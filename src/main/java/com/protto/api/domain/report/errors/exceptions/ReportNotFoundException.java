package com.protto.api.domain.report.errors.exceptions;

import com.protto.api.domain.report.errors.model.EReportErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class ReportNotFoundException extends AbstractErrorRuntimeException {

    public ReportNotFoundException(Long id) {
        super(
                "Report : %s does not exist",
                HttpStatus.NOT_FOUND,
                EReportErrorCode.REPORT_NOT_FOUND.value
        );
    }
}
