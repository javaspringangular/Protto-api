package com.protto.api.domain.report.errors.exceptions;


import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import com.protto.api.errors.exceptions.AbstractExceptionHandler;
import com.protto.api.errors.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ReportExceptionHandler extends AbstractExceptionHandler {

    @ExceptionHandler({
            ReportNotFoundException.class,
            ReportNoAccessException.class,
            ReportNoRightToCreateException.class,
            NoMatchBetweenUserLoggedAndUserInReportException.class
    })
    public ResponseEntity<ErrorResponse> customHandlerReport(AbstractErrorRuntimeException exception) {
        return handleException(exception);
    }
}
