package com.protto.api.domain.report.model.dao;

import java.time.LocalDate;

public record ReportInTableVO(
        Long id,
        String name,
        LocalDate date,
        LocalDate depositDate

) {
}
