package com.protto.api.domain.report.model.mapper;

import com.protto.api.data.entities.ReportEntity;
import com.protto.api.domain.report.model.dao.ReportInTableVO;
import com.protto.api.domain.report.model.dao.ReportVO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ReportMapper {

    ReportVO entityToReportVo(ReportEntity entity);

    @Mapping(source = "reportEntity.id", target = "id")
    @Mapping(source = "reportEntity.name", target = "name")
    @Mapping(source = "reportEntity.reportDate", target = "date")
    @Mapping(source = "reportEntity.depositDate", target = "depositDate")
    ReportInTableVO entityToReportInTableVO(ReportEntity reportEntity);
}
