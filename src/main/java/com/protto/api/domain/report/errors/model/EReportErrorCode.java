package com.protto.api.domain.report.errors.model;

public enum EReportErrorCode {

    REPORT_NOT_FOUND("report.not.found"),
    REPORT_NO_ACCESS("report.no.access"),
    REPORT_NO_RIGHTS_TO_CREATE("report.no.rights"),

    PDF_GENERATING_ERROR_USER_MATCH("pdf.generating.error.user.login");

    public final String value;

    EReportErrorCode(String value){
        this.value = value;
    }
}
