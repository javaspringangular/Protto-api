package com.protto.api.domain.report.errors.exceptions;

import com.protto.api.domain.report.errors.model.EReportErrorCode;
import com.protto.api.errors.exceptions.AbstractErrorRuntimeException;
import org.springframework.http.HttpStatus;

public class ReportNoRightToCreateException extends AbstractErrorRuntimeException {
    public ReportNoRightToCreateException() {
        super(
                "Pas de droits pour la création de rapport.",
                HttpStatus.FORBIDDEN,
                EReportErrorCode.REPORT_NO_RIGHTS_TO_CREATE.value
        );
    }
}
