package com.protto.api.domain.report.controller;

import com.protto.api.domain.report.model.dao.ReportCreateRequest;
import com.protto.api.domain.report.model.dao.ReportDateRequest;
import com.protto.api.domain.report.model.dao.ReportInTableVO;
import com.protto.api.domain.report.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping("/report")
public class ReportController {

    private final ReportService reportService;

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT)")
    public ResponseEntity<Long> create(@RequestBody @Valid ReportCreateRequest reportCreateRequest, Authentication loggedUser) {
        return ResponseEntity.status(HttpStatus.CREATED).body(reportService.create(reportCreateRequest, loggedUser));
    }

    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT)")
    public ResponseEntity<Void> delete(@PathVariable @Min(1) Long id, Authentication loggedUser) {
        reportService.delete(id, loggedUser);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).ADMIN)")
    public ResponseEntity<List<ReportInTableVO>> getAllReports() {
        return ResponseEntity.ok(reportService.getAll());
    }

    @GetMapping("/myReports")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT)")
    public ResponseEntity<List<ReportInTableVO>> getMyReports(Authentication loggedUser) {
        return ResponseEntity.ok(reportService.findMyReports(loggedUser));
    }

    @PostMapping("/myReports")
    @PreAuthorize("hasAnyAuthority(T(com.protto.api.domain.authentication.model.enums.ERole).CLIENT)")
    public ResponseEntity<List<ReportInTableVO>> getMyReportsFilterByDate(@RequestBody ReportDateRequest request, Authentication loggedUser) {
        return ResponseEntity.ok(reportService.findMyReportsFilterByProvidedDate(request, loggedUser));
    }
}
