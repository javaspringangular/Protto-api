package com.protto.api.domain.report.service;

import com.protto.api.data.entities.ReportEntity;
import com.protto.api.data.entities.UserEntity;
import com.protto.api.data.repositories.ReportRepository;
import com.protto.api.domain.pdf.errors.exception.FormatDateInReportException;
import com.protto.api.domain.report.errors.exceptions.NoMatchBetweenUserLoggedAndUserInReportException;
import com.protto.api.util.model.ModelParser;
import com.protto.api.domain.report.errors.exceptions.ReportNoAccessException;
import com.protto.api.domain.report.errors.exceptions.ReportNotFoundException;
import com.protto.api.domain.report.model.dao.ReportCreateRequest;
import com.protto.api.domain.report.model.dao.ReportDateRequest;
import com.protto.api.domain.report.model.dao.ReportInTableVO;
import com.protto.api.domain.report.model.dao.ReportVO;
import com.protto.api.domain.report.model.mapper.ReportMapper;
import com.protto.api.domain.user.service.UserService;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class ReportService {


    private final ReportRepository reportRepository;
    private final ReportMapper reportMapper;

    private final UserService userService;

    @Transactional(propagation = Propagation.REQUIRED)
    public Long create(ReportCreateRequest request, Authentication loggedUser){
        UserEntity user = userService.getUserByAuthentication(loggedUser);

        ReportEntity reportEntity = new ReportEntity();

        reportEntity.setName(normalizeReportName(user, request.reportDate()));
        reportEntity.setUserLogin(user.getLogin());
        reportEntity.setReportDate(request.reportDate());
        reportEntity.setDepositDate(LocalDate.now());
        reportEntity.setComExchanges(request.comExchanges());
        reportEntity.setSelfStudies(request.selfStudies());
        reportEntity.setProjects(request.projects());

        return reportRepository.save(reportEntity).getId();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Long create(Map<String, String> requestMap, Authentication loggedUser){
        UserEntity user = userService.getUserByAuthentication(loggedUser);
        String userLogin = requestMap.get(ModelParser.EMAIL_KEY);

        if(!match(user.getLogin(), userLogin))
            throw new NoMatchBetweenUserLoggedAndUserInReportException(userLogin);

        ReportEntity reportEntity = new ReportEntity();
        LocalDate reportDate;
        try {
            reportDate = LocalDate.parse(requestMap.get(ModelParser.REPORT_DATE_KEY));
        }catch(DateTimeException e) {
            throw new FormatDateInReportException("Format de la date invalid");
        }


        reportEntity.setName(normalizeReportName(user, reportDate));
        reportEntity.setUserLogin(user.getLogin());
        reportEntity.setReportDate(reportDate);
        reportEntity.setDepositDate(LocalDate.now());
        reportEntity.setComExchanges(requestMap.get(ModelParser.COM_KEY));
        reportEntity.setSelfStudies(requestMap.get(ModelParser.SELF_KEY));
        reportEntity.setProjects(requestMap.get(ModelParser.PROJECT_KEY));

        return reportRepository.save(reportEntity).getId();
    }

    private boolean match(String login, String user) {
        return login.equals(user);
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<ReportInTableVO> getAll() {
        return StreamSupport.stream(reportRepository.findAll().spliterator(), false)
                .map(reportMapper::entityToReportInTableVO)
                .toList();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Long id, Authentication loggedUser) {
        if(!hasAccess(id, loggedUser))
            throw new ReportNoAccessException(id);
        try{
            reportRepository.deleteById(id);
        }catch (EmptyResultDataAccessException exception) {
            throw new ReportNotFoundException(id);
        }
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public ReportVO getReport(Long id) {
        return reportRepository.findById(id)
                .map(reportMapper::entityToReportVo)
                .orElseThrow(() -> new ReportNotFoundException(id));
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<ReportInTableVO> findMyReports(Authentication loggedUser) {
        UserEntity user = userService.getUserByAuthentication(loggedUser);

        return reportRepository.findReportsByLogin(user.getLogin())
                .stream()
                .map(reportMapper::entityToReportInTableVO)
                .toList();
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<ReportInTableVO> findMyReportsFilterByProvidedDate(ReportDateRequest request, Authentication loggedUser) {
        UserEntity user = userService.getUserByAuthentication(loggedUser);

        LocalDate start = LocalDate.of(request.year(), request.month(), 1);
        LocalDate end = start.plusMonths(1);

        return reportRepository.findReportsByLoginFilterByDateBetween(user.getLogin(), start, end)
                .stream()
                .map(reportMapper::entityToReportInTableVO)
                .toList();
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public String getNameFromReport(Long id) {
        return reportRepository.findReportNameByReportId(id).orElseThrow(
                () -> new ReportNotFoundException(id)
        );
    }

    private boolean hasAccess(Long id, Authentication loggedUser) {
        if(userService.isAdmin(loggedUser))
            return true;

        UserEntity user = userService.getUserByAuthentication(loggedUser);
        Optional<ReportEntity> report = reportRepository.findById(id);

        if (report.isEmpty()) {
            throw new ReportNotFoundException(id);
        }

        return Objects.equals(report.get().getUserLogin(), user.getLogin());
    }

    public boolean hasAccessP(Long id, Authentication loggedUser) {
        if(userService.isAdmin(loggedUser))
            return true;

        UserEntity user = userService.getUserByAuthentication(loggedUser);

        return reportRepository.findById(id)
                .map(reportMapper::entityToReportVo)
                .stream()
                .anyMatch(reportVO -> reportVO.userLogin().equals(user.getLogin()));
    }

    private String normalizeReportName(UserEntity user, LocalDate reportDate) {
        return "weekly_report_" + user.getLastName() + "_" + user.getFirstName() + '_' + reportDate.toString();
    }
}
