package com.protto.api.domain.report.model.dao;

import javax.validation.constraints.NotEmpty;

public record ReportDateRequest(
        @NotEmpty int year,
        @NotEmpty int month
) {
}
