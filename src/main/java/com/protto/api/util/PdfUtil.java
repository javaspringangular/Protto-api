package com.protto.api.util;

import com.itextpdf.text.DocumentException;
import com.protto.api.domain.pdf.errors.exception.ErrorGeneratingPdfFileException;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PdfUtil {

    private PdfUtil() {
        throw new IllegalStateException("Classe utilitaire, non instanciable.");
    }
    public static byte[] generatePdfFromHtmlContent(String htmlContent) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            renderer.createPDF(out);
            renderer.finishPDF();
        } catch (DocumentException | IOException e) {
            throw new ErrorGeneratingPdfFileException();
        }
        return out.toByteArray();
    }
}
