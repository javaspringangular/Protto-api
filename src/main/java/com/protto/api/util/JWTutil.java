package com.protto.api.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class JWTutil {

    @Value("${security.token.secret}")
    private String tokenSecret =
            "france47HGEILQJsdnie(utjfpzhg4hsg24J!DF9lDIpSJAGjfsgrytcb54hkSDGRbnCGHSU!KSJHP0jsurtYUEjS";

    @Value("${security.token.expiration.time}")
    private long expirationTime = 3600000;

    private final Algorithm algorithm;
    private final JWTVerifier jwtVerifier;

    public static final String ROLES_KEY = "roles";


    public JWTutil() {
        algorithm = Algorithm.HMAC256(tokenSecret);
        jwtVerifier = JWT.require(algorithm)
                .withIssuer("Protto")
                .build();
    }

    public String generateToken(UserDetails user) {
        return generateToken(user.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList(), user.getUsername());
    }

    public String generateToken(List<String> claims, String username) {
        return JWT.create()
                .withIssuer("Protto")
                .withIssuedAt(new Date())
                .withClaim(ROLES_KEY, claims)
                .withSubject(username)
                .withExpiresAt(new Date((new Date()).getTime() + expirationTime))
                .sign(algorithm);
    }

    public DecodedJWT getDecodedToken(String token) {
        return jwtVerifier.verify(token);
    }

    public List<SimpleGrantedAuthority> getAuthoritiesFromToken(String token) {
        return getDecodedToken(token).getClaim(ROLES_KEY)
                        .asList(String.class)
                        .stream()
                        .map(SimpleGrantedAuthority::new)
                        .toList();

    }

    public String getUserNameFromToken(String token) {
        return getDecodedToken(token).getSubject();
    }

    public boolean validateToken(String authToken) {
        try {
            getDecodedToken(authToken);
            return true;
        } catch (JWTDecodeException e) {
            log.error("Signature invalide: {}", e.getMessage());
            throw e;
        }

    }

}
