package com.protto.api.util.model;

public class ModelParser {

    private ModelParser(){
        throw new IllegalStateException("Utility class");
    }

    public static final String EMAIL_KEY = "Email :";
    public static final String REPORT_DATE_KEY = "* DATE DU RAPPORT :";
    public static final String TITLE_KEY = "Rapport Hebdomadaire";
    public static final String LASTNAME_KEY = "Nom :";
    public static final String FIRSTNAME_KEY = "Prénom :";
    public static final String COM_KEY = "* Echanges commerciaux *";
    public static final String SELF_KEY = "* Sujets d'auto-formation *";
    public static final String PROJECT_KEY = "* Projets durant la période d'intercontrat *";
    public static final String EOF = "Informations importantes";

}