package com.protto.api.util;

import com.protto.api.domain.pdf.parser.service.Parser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;

public class FileUtil {
    private static final Path root = Paths.get("uploads");

    private FileUtil() {
        throw new IllegalStateException("Classe utilitaire, non instanciable.");
    }

    public static HttpHeaders generateHeaders(byte[] isr, String fileName, String contentType) {
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentLength(isr.length);
        respHeaders.set(HttpHeaders.CONTENT_TYPE, contentType);
        respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        respHeaders.add("Access-Control-Expose-Headers", "Content-disposition");
        return respHeaders;
    }

    public static void deleteFile(String fileName) throws IOException {
        Files.deleteIfExists(root.resolve(fileName));
    }

    public static Map<String, String> parseFile(String originalFilename) throws IOException {
        File file = new File(String.valueOf(root.resolve(Objects.requireNonNull(originalFilename))));
        try(PDDocument document = PDDocument.load(file)) {
            PDFTextStripper stripper = new PDFTextStripper();

            return Parser.parse(stripper.getText(document));
        }
    }

    public static void copy(MultipartFile file, String fileName) throws IOException {
        Files.copy(file.getInputStream(), root.resolve(Objects.requireNonNull(fileName)));
    }
}
