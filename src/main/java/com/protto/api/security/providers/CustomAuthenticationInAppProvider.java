package com.protto.api.security.providers;


import com.protto.api.security.details.UserDetailsForAuthentication;
import com.protto.api.security.service.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CustomAuthenticationInAppProvider implements AuthenticationProvider {

    private final CustomUserDetailsService customUserDetailsService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();

        UserDetailsForAuthentication user = (UserDetailsForAuthentication) customUserDetailsService.loadUserByUsername(username);

        if(user == null){
            return null;
        }

        if (BCrypt.checkpw(password, user.getPassword())) {
            return new UsernamePasswordAuthenticationToken(
                    user, null, user.getAuthorities());
        }

        throw new BadCredentialsException("Bad Credentials");

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
