package com.protto.api.security.providers;


import com.protto.api.security.details.TokenAuthentication;
import com.protto.api.security.details.UserDetailsForAuthentication;
import com.protto.api.util.JWTutil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CustomAuthenticationTokenProvider implements AuthenticationProvider {

    private final JWTutil jwTutil;

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String token = authentication.getCredentials().toString();

        if(jwTutil.validateToken(token)) {
            String username = jwTutil.getUserNameFromToken(token);

            UserDetailsForAuthentication user = UserDetailsForAuthentication.builder()
                    .username(username)
                    .authorities(jwTutil.getAuthoritiesFromToken(token))
                    .build();

            return new TokenAuthentication(
                    user, null, user.getAuthorities());

        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(TokenAuthentication.class);
    }
}
