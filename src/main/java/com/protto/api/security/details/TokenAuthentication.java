package com.protto.api.security.details;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class TokenAuthentication extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 590L;
    private final Object principal;
    private Object credentials;

    public TokenAuthentication(Object credentials) {
        super(null);
        this.principal = null;
        this.credentials = credentials;
        super.setAuthenticated(false);
    }

    public TokenAuthentication(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities ) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }


}
