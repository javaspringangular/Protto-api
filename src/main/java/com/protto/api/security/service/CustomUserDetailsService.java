package com.protto.api.security.service;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.domain.user.service.UserService;
import com.protto.api.security.details.UserDetailsForAuthentication;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;


    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<UserEntity> user = userService.getUserByUserName(username);

        if(user.isEmpty()){
            throw new BadCredentialsException("Bad Credentials");
        }

        return UserDetailsForAuthentication.builder()
                .username(user.get().getLogin())
                .password(user.get().getPassword())
                .authorities(List.of(new SimpleGrantedAuthority(user.get().getRole().toString())))
                .build();
    }


}
