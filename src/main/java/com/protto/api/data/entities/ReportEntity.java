package com.protto.api.data.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_weekly_report")
public class ReportEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_id")
    private Long id;

    @Column(name = "report_name")
    private String name;

    @Column(name = "report_date")
    private LocalDate reportDate;

    @Column(name = "report_deposit_date")
    private LocalDate depositDate;

    @Column(name = "report_user_login")
    private String userLogin;

    @Column(name = "report_com_exchanges")
    private String comExchanges;

    @Column(name = "report_self_studies")
    private String selfStudies;

    @Column(name = "report_projects")
    private String projects;

}
