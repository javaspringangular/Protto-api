package com.protto.api.data.repositories;

import com.protto.api.data.entities.ReportEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ReportRepository extends CrudRepository<ReportEntity, Long> {


    @Query(value = """
        select r
        from ReportEntity r
        where r.userLogin = :login
    """)
    List<ReportEntity> findReportsByLogin(@Param("login")String login);

    @Query(value = """
        select r
        from ReportEntity r
        where r.userLogin = :login
        and r.reportDate between :start and :end
    """)
    List<ReportEntity> findReportsByLoginFilterByDateBetween(@Param("login")String login,@Param("start") LocalDate start,@Param("end") LocalDate end);


    @Query(value = """
        select r.name
        from ReportEntity r
        where r.id =:id
    """)
    Optional<String> findReportNameByReportId(@Param("id") Long id);
}
