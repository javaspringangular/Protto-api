package com.protto.api.config;

import com.protto.api.security.AuthTokenFilter;
import com.protto.api.security.providers.CustomAuthenticationInAppProvider;
import com.protto.api.security.providers.CustomAuthenticationTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfigurationSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AppSecurityConfig {

    @Autowired
    private CustomAuthenticationInAppProvider authInAppProvider;

    @Autowired
    private CustomAuthenticationTokenProvider authTokenProvider;

    @Autowired
    @Qualifier("corsConfig")
    CorsConfigurationSource corsConfigurationSource;

    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        http
                .cors().configurationSource(corsConfigurationSource).and().csrf().disable()
                .authenticationManager(authenticationManager(http))
                .addFilterBefore(authenticationWebFilter(), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "**").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/user/create").permitAll()
                .antMatchers("/h2-console", "/h2-console/**").permitAll()
                .anyRequest().authenticated();
        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity http) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.authenticationProvider(authInAppProvider);
        authenticationManagerBuilder.authenticationProvider(authTokenProvider);
        return authenticationManagerBuilder.build();
    }

    @Bean
    public AuthTokenFilter authenticationWebFilter() {
        return new AuthTokenFilter();
    }

}
