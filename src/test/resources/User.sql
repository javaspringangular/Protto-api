-----INSERT USER-----

INSERT INTO t_user (user_id, user_first_name, user_last_name, user_login, user_password, user_role) VALUES (1L, 'Ottman', 'Becuwe', 'dataForTest1@gmail.com', '$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O', 'ADMIN');
INSERT INTO t_user (user_id,user_first_name, user_last_name, user_login, user_password, user_role) VALUES (2L, 'William', 'Becuwe', 'dataForTest2@gmail.com', '$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O', 'CLIENT');
INSERT INTO t_user (user_id, user_first_name, user_last_name, user_login, user_password, user_role) VALUES (3L, 'Alexandre', 'Becuwe', 'dataForTest3@gmail.com', '$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O', 'CLIENT');