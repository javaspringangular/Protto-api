package com.protto.api.security.service;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.domain.user.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class CustomUserDetailsServiceTest {

    @MockBean
    UserService userService;
    CustomUserDetailsService customUserDetailsService;

    private final UserEntity user = UserEntity.builder()
            .id(1L)
            .login("dataForTest1@gmail.com")
            .password("$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O")
            .firstName("Ottman")
            .lastName("Becuwe")
            .role(ERole.ADMIN)
            .build();
    @BeforeEach
    void setup() {
        customUserDetailsService = new CustomUserDetailsService(userService);
    }

    @Test
    void whenLoadUserByWrongUsername_thenShouldReturnBadCredentialException() {
        Mockito.when(userService.getUserByUserName(user.getLogin())).thenReturn(Optional.empty());


        assertThatThrownBy(() -> customUserDetailsService.loadUserByUsername("dataForTest1@gmail.com"))
                .isInstanceOf(BadCredentialsException.class);
    }

    @Test
    void whenLoadUserByUsername_thenShouldSuccessAnd() {
        Mockito.when(userService.getUserByUserName(user.getLogin())).thenReturn(Optional.of(user));

        UserDetails expectedUserDetails = customUserDetailsService.loadUserByUsername(user.getLogin());
        assertThat(expectedUserDetails.getUsername()).isEqualTo(user.getLogin());
        assertThat(expectedUserDetails.getAuthorities()).asString().isEqualTo("[ADMIN]");
        assertThat(expectedUserDetails.getPassword()).isEqualTo(user.getPassword());
    }
}
