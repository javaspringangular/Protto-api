package com.protto.api.security;

import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.security.details.TokenAuthentication;
import com.protto.api.security.details.UserDetailsForAuthentication;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@ExtendWith(SpringExtension.class)
public class AuthTokenFilterTest {

    @Mock
    AuthenticationManager authenticationManager;

    @InjectMocks
    AuthTokenFilter tokenFilter;

    @Test
    void doFilterInternalWithoutToken_then_should_notAuthenticateAndFilter() throws ServletException, IOException {

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        FilterChain filterChain = Mockito.mock(FilterChain.class);


        Mockito.when(request.getHeader("Authorization")).thenReturn(null);
        Mockito.doNothing().when(filterChain).doFilter(request, response);

        tokenFilter.doFilterInternal(request, response, filterChain);

        Assertions.assertThat(SecurityContextHolder.getContext().getAuthentication())
                .isNull();

    }

    @Test
    void doFilterInternalWithToken_then_should_authenticateAndFilter() throws ServletException, IOException {

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        FilterChain filterChain = Mockito.mock(FilterChain.class);


        Mockito.when(request.getHeader("Authorization"))
                .thenReturn("Bearer tokengood");

        Mockito.when(authenticationManager.authenticate(Mockito.any()))
                .thenReturn(new TokenAuthentication(
                        UserDetailsForAuthentication.builder().username("michel").authorities(List.of(new SimpleGrantedAuthority(ERole.ADMIN.name()))).build(),
                        null,
                        List.of(new SimpleGrantedAuthority(ERole.ADMIN.name()))));

        Mockito.doNothing().when(filterChain)
                .doFilter(request, response);

        tokenFilter.doFilterInternal(request, response, filterChain);

        Assertions.assertThat(SecurityContextHolder.getContext().getAuthentication())
                .isNotNull();

    }

}
