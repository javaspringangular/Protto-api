package com.protto.api.security.providers;

import com.protto.api.security.details.TokenAuthentication;
import com.protto.api.security.details.UserDetailsForAuthentication;
import com.protto.api.util.JWTutil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class CustomAuthenticationTokenProviderTest {

    @Mock
    JWTutil jwTutil;

    @InjectMocks
    CustomAuthenticationTokenProvider tokenProvider;

    @Test
    void whenHaveToken_then_should_SuccessAuthentication(){
        Mockito.when(jwTutil.validateToken(Mockito.anyString())).thenReturn(true);
        Mockito.when(jwTutil.getUserNameFromToken(Mockito.anyString())).thenReturn("michel");

        Assertions.assertThat(((UserDetailsForAuthentication)
                        tokenProvider.authenticate(new TokenAuthentication("tokenvalide")).getPrincipal()).getUsername())
                .isEqualTo("michel");
    }

    @Test
    void whenNoToken_then_should_failAuthentication(){
        Mockito.when(jwTutil.validateToken(Mockito.anyString())).thenReturn(false);

        Assertions.assertThat(((UserDetailsForAuthentication) tokenProvider.authenticate(new TokenAuthentication("tokenNotValide")))).isNull();

    }
}

