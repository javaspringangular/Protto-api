package com.protto.api.security.providers;

import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.security.details.UserDetailsForAuthentication;
import com.protto.api.security.service.CustomUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(SpringExtension.class)
class CustomAuthenticationInAppProviderTest {

    @Mock
    CustomUserDetailsService customUserDetailsService;

    @InjectMocks
    CustomAuthenticationInAppProvider customAuthenticationInAppProvider;

    UserDetailsForAuthentication client1;

    UsernamePasswordAuthenticationToken detailsAuthTokenGoodCredentials;
    UsernamePasswordAuthenticationToken detailsAuthTokenBadCredentials;

    @BeforeEach
    void setUp() {
        client1 = new UserDetailsForAuthentication(
                "dataForTest1@gmail.com",
                "$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O",
                List.of(new SimpleGrantedAuthority(ERole.CLIENT.name()))
        );

        detailsAuthTokenGoodCredentials = new UsernamePasswordAuthenticationToken("dataForTest1@gmail.com", "Tototo77!", client1.getAuthorities());
        detailsAuthTokenBadCredentials = new UsernamePasswordAuthenticationToken("dataForTest1@gmail.com", "", client1.getAuthorities());
    }

    @Test
    void when_authenticateInApp_then_should_getUsernamePasswordAuthenticationToken() {

        Mockito.when(customUserDetailsService.loadUserByUsername(Mockito.any())).thenReturn(client1);

        Authentication auth = customAuthenticationInAppProvider.authenticate(detailsAuthTokenGoodCredentials);
        UserDetailsForAuthentication res = (UserDetailsForAuthentication) auth.getPrincipal();

        assertThat(res.getUsername()).hasToString("dataForTest1@gmail.com");
    }

    @Test
    void when_authenticateInApp_then_should_throwBadCredentialsException() {

        Mockito.when(customUserDetailsService.loadUserByUsername(Mockito.any())).thenReturn(client1);

        assertThatThrownBy(
                () -> customAuthenticationInAppProvider.authenticate(detailsAuthTokenBadCredentials))
                .isInstanceOf(BadCredentialsException.class);
    }

    @Test
    void when_authenticateInApp_then_should_returnNull() {

        Mockito.when(customUserDetailsService.loadUserByUsername(Mockito.any())).thenReturn(null);

        assertThat(customAuthenticationInAppProvider.authenticate(detailsAuthTokenGoodCredentials)).isNull();
    }


}
