package com.protto.api.domain.user.service;


import com.protto.api.data.entities.UserEntity;
import com.protto.api.data.repositories.UserRepository;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.domain.user.errors.exceptions.UserAlreadyExistException;
import com.protto.api.domain.user.errors.exceptions.UserLoginPatternException;
import com.protto.api.domain.user.errors.exceptions.UserNotFoundException;
import com.protto.api.domain.user.model.dao.UserCreateRequest;
import com.protto.api.domain.user.model.dao.UserVO;
import com.protto.api.domain.user.model.mapper.UserMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.core.Authentication;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;


import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserServiceTest {

    @Autowired
    private UserRepository userRepository;
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private UserService userService;

    @BeforeEach
    void setUp() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        userService = new UserService(userRepository, userMapper, encoder);
    }

    @Test
    @Sql("/User.sql")
    void whenGetAllUser_thenShouldReturnGoodSizeList() {
        List<UserVO> userVOList = userService.getAllUsers();

        assertNotNull(userVOList);
        assertThat(userVOList).hasSize(3); // 3 file sql
    }

    @Test
    @Sql("/User.sql")
    void whenFindUserById_thenShouldExpectGoodUser() {
        UserVO userVO = userService.findById(2L);

        assertThat(userVO.firstName()).isEqualTo("William");
        assertThat(userVO.lastName()).isEqualTo("Becuwe");
    }

    @Test
    @Sql("/User.sql")
    void whenFindUserName_thenShouldExpectGoodUser() {
        Optional<UserEntity> user = userService.getUserByUserName("dataForTest1@gmail.com");
        if(user.isPresent()){
            assertThat(user.get().getFirstName()).isEqualTo("Ottman");
            assertThat(user.get().getLastName()).isEqualTo("Becuwe");
        }
    }

    @Test
    @Sql("/User.sql")
    void whenCreateUserAsResourceManager_thenShouldSuccess() {
        UserCreateRequest userCreateRequest = new UserCreateRequest("Robs", "Becuwe", "dataForTest4@solutec.fr", "Ouiouioui77");

        assertThat(userService.create(userCreateRequest)).isEqualTo(4L);
    }
    @Test
    @Sql("/User.sql")
    void whenCreateUserAsConsultant_thenShouldSuccess() {
        UserCreateRequest userCreateRequest = new UserCreateRequest("Robs", "Becuwe", "dataForTest4@consultant-solutec.fr", "Ouiouioui77");

        assertThat(userService.create(userCreateRequest)).isEqualTo(5L);
    }

    @Test
    @Sql("/User.sql")
    void whenCreateUser_thenShouldThrowError() {
        UserCreateRequest userCreateRequest = new UserCreateRequest("Willywill", "Becuwe", "dataForTest2@gmail.com", "Ouiouioui77");

        assertThatThrownBy(() -> userService.create(userCreateRequest))
                .isInstanceOf(UserAlreadyExistException.class);
    }

    @Test
    @Sql("/User.sql")
    void whenCreateUser_thenShouldThrowErrorCauseLoginPatternException() {
        UserCreateRequest userCreateRequest = new UserCreateRequest("toto", "Becuwe", "toto@gmail.com", "Ouiouioui77");

        assertThatThrownBy(() -> userService.create(userCreateRequest))
                .isInstanceOf(UserLoginPatternException.class);
    }
    @Test
    @Sql("/User.sql")
    void whenFindUserById_thenShouldThrowError() {

        assertThatThrownBy(() -> userService.findById(5L))
                .isInstanceOf(UserNotFoundException.class);
    }
    @Test
    @Sql("/User.sql")
    void whenFindUserById0L_thenShouldThrowError() {

        assertThatThrownBy(() -> userService.findById(0L))
                .isInstanceOf(UserNotFoundException.class);
    }

    @Test
    @Sql("/User.sql")
    void giveAlreadyRegisteredUser_then_should_returnAlreadyRegistered() {
        assertTrue(userService.isAlreadyRegistered("dataForTest1@gmail.com"));
    }

    @Test
    @Sql("/User.sql")
    void giveAlreadyRegisteredUser_then_should_returnNotAlreadyRegistered() {
        assertFalse(userService.isAlreadyRegistered("notRegisteredUser@gmail.com"));
    }

    @Test
    @Sql("/User.sql")
    void givenLogin_then_should_returnGoodUser() {
        UserEntity userToFound = userService.getUserByUserName("dataForTest1@gmail.com").get();

        assertThat(userToFound.getFirstName()).isEqualTo("Ottman");
        assertThat(userToFound.getLastName()).isEqualTo("Becuwe");
        assertThat(userToFound.getId()).isEqualTo(1L);
    }

    @Test
    void givenConsultant_then_should_setRoleAsClient() {
        assertThat(userService.getRoleFromLogin("toto@consultant-solutec.fr")).isEqualTo(ERole.CLIENT);
    }

    @Test
    void givenResourceManager_then_should_setRoleAsAdmin() {
        assertThat(userService.getRoleFromLogin("tata@solutec.fr")).isEqualTo(ERole.ADMIN);
    }

    @Test
    void givenWrongPatternEmail_then_should_throwError() {
        Assertions.assertThatThrownBy(() ->
                userService.getRoleFromLogin("tata@toto.fr")
                ).isInstanceOf(UserLoginPatternException.class);
    }

    @Test
    @Sql("/User.sql")
    void whenGetUserByUserName_then_should_returnGoodUser() {
        assertThat(userService.getUserByUserName("dataForTest1@gmail.com").get().getFirstName()).isEqualTo("Ottman");
    }

    @Test
    @Sql("/User.sql")
    @WithMockUser(value = "willy", authorities = {"ADMIN"})
    void whenIsAdmin_then_should_returnTrue() {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        assertTrue(userService.isAdmin(loggedUser));
    }

    @Test
    @Sql("/User.sql")
    @WithMockUser(value = "willy", authorities = {"CLIENT"})
    void whenIsAdmin_then_should_returnFalse() {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        assertFalse(userService.isAdmin(loggedUser));
    }

    @Test
    @Sql("/User.sql")
    @WithMockUser(value = "willy", authorities = {"CLIENT"})
    void whenIsClient_then_should_returnTrue() {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        assertTrue(userService.isClient(loggedUser));
    }

    @Test
    @Sql("/User.sql")
    @WithMockUser(value = "willy", authorities = {"ADMIN"})
    void whenIsClient_then_should_returnFalse() {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        assertFalse(userService.isClient(loggedUser));
    }
    @Test
    @Sql("/User.sql")
    @WithMockUser(value = "dataForTest1@gmail.com", authorities = {"ADMIN"})
    void whenGetUserByAuthentication_then_should_returnGoodUser() {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        UserEntity user = userService.getUserByAuthentication(loggedUser);
        assertThat(user.getFirstName()).isEqualTo("Ottman");
        assertThat(user.getLastName()).isEqualTo("Becuwe");
    }

    @Test
    @Sql("/User.sql")
    @WithMockUser(value = "wrongUser@gmail.com", authorities = {"ADMIN"})
    void whenGetUserByAuthentication_then_should_throwError() {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        assertThatThrownBy(() -> userService.getUserByAuthentication(loggedUser))
                .isInstanceOf(UserNotFoundException.class);
    }


}
