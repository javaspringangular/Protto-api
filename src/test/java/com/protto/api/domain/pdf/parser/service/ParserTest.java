package com.protto.api.domain.pdf.parser.service;

import com.protto.api.domain.pdf.parser.errors.exception.IllegalDataInDocumentException;
import com.protto.api.domain.pdf.parser.errors.exception.IllegalKeyValueException;
import com.protto.api.util.model.ModelParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class ParserTest {

    @Test
    void testPrivateConstructor() {
        Constructor<Parser> constructor;
        try {
            constructor = Parser.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            constructor.newInstance();

            fail("Expected exception not thrown");
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException e) {
            fail("Unexpected exception: " + e);
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();

            Assertions.assertEquals(IllegalStateException.class, cause.getClass(), "Unexpected exception thrown");
            Assertions.assertEquals("Utility class", cause.getMessage(), "Unexpected exception message");
        }
    }

    @Test
    void whenParseDocument_then_shouldReturnGoodParsedMap() {
        String documentTestSuccess = """
                Email : willy@gmail.com * DATE DU RAPPORT : 2023-03-05 \s
                Nom : BECUWE \s
                Prénom : William \s
                \s
                \s
                Rapport Hebdomadaire\s
                \s
                \s
                * Echanges commerciaux * \s
                \s
                \s
                - Liam TOTORO\s
                - Toto COCORICO\s
                - Aziz BOUTEFLICA\s
                \s
                \s
                * Sujets d'auto-formation * \s
                \s
                - Java spring boot\s
                - SQL postgres\s
                - JPA LIQUIBASE HIbernates\s
                - dfgsdfsdf\s
                \s
                \s
                \s
                * Projets durant la période d'intercontrat * \s
                \s
                \s
                Ouais bg que ca fait des contrat de fou la, c’est du gros projet hein. \s
                Protto il regale, c’est la ré ga la de   !\s
                \s
                \s
                Informations importantes  \s
                * Format de la date : YYYY-MM-DD\s
                """;

        Map<String,String> parsedMap = Parser.parse(documentTestSuccess);

        assertThat(parsedMap.get(ModelParser.EMAIL_KEY)).isEqualTo("willy@gmail.com");
        assertThat(parsedMap.get(ModelParser.LASTNAME_KEY)).isEqualTo("BECUWE");
        assertThat(parsedMap.get(ModelParser.FIRSTNAME_KEY)).isEqualTo("William");
    }

    @Test
    void whenParseDocument_then_shouldThrowIllegalKeyValueException() {
        String documentTestFailKeyValueMissing = """
                Email : willy@gmail.com -03-05\s
                Nom : BECUWE \s
                Prénom : William \s
                \s
                \s
                Rapport Hebdomadaire\s
                \s
                \s
                * Echanges commerciaux * \s
                \s
                \s
                - Liam TOTORO\s
                - Toto COCORICO\s
                - Aziz BOUTEFLICA\s
                \s
                \s
                * Sujets d'auto-formation * \s
                \s
                - Java spring boot\s
                - SQL postgres\s
                - JPA LIQUIBASE HIbernates\s
                - dfgsdfsdf\s
                \s
                \s
                \s
                * Projets durant la période d'intercontrat * \s
                \s
                \s
                Ouais bg que ca fait des contrat de fou la, c’est du gros projet hein. \s
                Protto il regale, c’est la ré ga la de   !\s
                \s
                \s
                Informations importantes  \s
                * Format de la date : YYYY-MM-DD\s
                """;

        assertThatThrownBy(() -> Parser.parse(documentTestFailKeyValueMissing))
                .isInstanceOf(IllegalDataInDocumentException.class);
    }

    @Test()
    void testIllegalKeyValueException() {
        String document = """
                Email : email@example.com \s
                * DATE DU RAPPORT : 2023-03-03
                Nom : Toto \s
                Prénom : tata \s
                Rapport Hebdomadaire \s
                Contenu du rapport... \s
                * Echanges commerciaux * \s
                Contenu des échanges... \s
                * Sujets d'auto-formation * \s
                Contenu des sujets... \s
                * Projets durant la période d'intercontrat * \s
                Contenu des projets... \s
                Informations importantes""";

        try {
            Parser.getRange("UNKNOWN_KEY", document);
            fail("Expected IllegalKeyValueException was not thrown.");
        } catch (IllegalKeyValueException e) {
            // Exception thrown, test passed.
        }
    }
}
