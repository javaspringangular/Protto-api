package com.protto.api.domain.pdf.controller;

import com.protto.api.domain.pdf.service.ReportPdfService;
import com.protto.api.domain.report.service.ReportService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReportPdfController.class)
@AutoConfigureMockMvc(addFilters = false)
class ReportPdfControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ReportService reportService;
    @MockBean
    private ReportPdfService reportPdfService;

    @Test
    void whenUploadReport_then_shouldSuccess() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "test.pdf", "application/pdf", "test data".getBytes());

        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();

        mockMvc.perform(MockMvcRequestBuilders.multipart("/report/uploadReport")
                        .file(file))
                .andExpect(status().isCreated())
                        .andReturn();

        verify(reportPdfService).uploadReport(file, loggedUser);
    }


    @Test
    void whenGenerateReport_then_shouldSuccess() throws Exception {
        byte[] fileData = "test data".getBytes();
        long id = 1L;
        String reportName = "test.pdf";

        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();

        when(reportPdfService.generateReportPdf(id, loggedUser)).thenReturn(fileData);
        when(reportService.getNameFromReport(anyLong())).thenReturn(reportName);

        MvcResult result = mockMvc.perform(get("/report/pdf/" + id))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse response = result.getResponse();

        Assertions.assertEquals(MediaType.APPLICATION_PDF_VALUE, response.getContentType());
        Assertions.assertEquals(response.getContentAsByteArray().length, fileData.length);
        assertThat(Objects.requireNonNull(response.getHeader("Content-Disposition")).split(";")[1].split("=")[1]).isEqualTo(reportName);

        verify(reportPdfService).generateReportPdf(1L, loggedUser);
        verify(reportService).getNameFromReport(1L);
    }

    @Test
    void whenGeneratePdfTemplate_then_shouldSuccess() throws Exception {
        byte[] fileData = "test data".getBytes();

        when(reportPdfService.getReportTemplate()).thenReturn(fileData);

        MvcResult result = mockMvc.perform(get("/report/template"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse response = result.getResponse();

        Assertions.assertEquals(MediaType.APPLICATION_PDF_VALUE, response.getContentType());
        Assertions.assertEquals(response.getContentAsByteArray().length, fileData.length);
        assertThat(Objects.requireNonNull(response.getHeader("Content-Disposition")).split(";")[1].split("=")[1]).isEqualTo("reportTemplate.pdf");

        verify(reportPdfService).getReportTemplate();
    }

}
