package com.protto.api.domain.pdf.service;

import com.protto.api.domain.report.service.ReportService;
import com.protto.api.domain.user.model.mapper.UserMapper;
import com.protto.api.domain.user.service.UserService;
import org.junit.jupiter.api.BeforeEach;

import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.thymeleaf.TemplateEngine;
@DataJpaTest
class ReportPdfServiceTest {

    @Mock
    private UserService userService;
    @Mock
    private TemplateEngine templateEngine;
    @Mock
    private ReportService reportService;
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    private ReportPdfService reportPdfService;

    @BeforeEach
    void setup() {
          this.reportPdfService = new ReportPdfService(
                templateEngine,
                userService,
                reportService,
                userMapper
        );
    }
}