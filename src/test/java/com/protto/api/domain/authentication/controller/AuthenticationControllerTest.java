package com.protto.api.domain.authentication.controller;


import com.protto.api.data.entities.UserEntity;
import com.protto.api.domain.authentication.model.dao.AuthenticationRequest;
import com.protto.api.domain.authentication.model.dao.AuthenticationResponse;
import com.protto.api.domain.authentication.model.dao.ResetPasswordRequest;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.domain.authentication.service.AuthenticationService;
import com.protto.api.domain.user.errors.exceptions.UserNotFoundException;
import com.protto.api.security.details.UserDetailsForAuthentication;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import com.protto.api.TestUtil;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthenticationController.class)
@AutoConfigureMockMvc(addFilters = false)
class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AuthenticationService authService;

    UserEntity client1Ent;

    UserDetailsForAuthentication client1;

    @BeforeEach
    void setUp() {
        client1 = new UserDetailsForAuthentication(
                "ottinestv@gmail.com", "", List.of(new SimpleGrantedAuthority(ERole.CLIENT.name())));

        client1Ent = UserEntity.builder()
                .id(1L).login("ottinestv@gmail.com")
                .password("$2a$10$7VdOGXkh7W3f6mYKfCrcR.yMUn31pKvMc7xBtuZ5/oNpB6y.NxN0O")
                .role(ERole.CLIENT)
                .build(); //Tototo77!
    }

    @Test
    void whenLoginWithInAppGoodCredentials_then_should_getToken() throws Exception {
        AuthenticationRequest request = new AuthenticationRequest("ottinestv@gmail.com", "Tototo77!");

        Authentication authentication = Mockito.mock(Authentication.class);
        Mockito.when(authentication.getPrincipal()).thenReturn(client1);
        Mockito.when(authentication.isAuthenticated()).thenReturn(true);

        Mockito.when(authService.login(Mockito.any()))
                .thenReturn(new AuthenticationResponse("token"));

        MvcResult result = mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isOk())
                .andReturn();

        AuthenticationResponse response = TestUtil.parseJson(result.getResponse().getContentAsString(), AuthenticationResponse.class).get(0);

        Assertions.assertThat(response.token())
                .isEqualTo("token");
    }

    @Test
    void whenAuthenticationWrongPassword_then_should_throwError() throws Exception {

        AuthenticationRequest request = new AuthenticationRequest("ottinestv@gmail.com", "wrongpass");

        Mockito.when(authService.login(Mockito.any())).thenThrow(new BadCredentialsException("Bad credential"));

        mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isForbidden());

    }

    @Test
    void whenLoginUsernameEmpty_then_should_throwError() throws Exception {
        AuthenticationRequest request = new AuthenticationRequest("", "Tototo77!");

        mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void whenLoginPasswordEmpty_then_should_throwError() throws Exception {
        AuthenticationRequest request = new AuthenticationRequest("ottinestv@gmail.com", "");

        mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void whenResetPassword_then_shouldSuccess() throws Exception {
        ResetPasswordRequest request = new ResetPasswordRequest("dataForTest1@gmail.com", "0123456789");

        Mockito.doNothing()
                .when(authService).resetPassword(Mockito.any());

        mockMvc.perform(post("/auth/resetPassword")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.toJSON(request)))
                .andExpect(status().isOk());
    }

    @Test
    void whenResetPassword_then_shouldThrowError() throws Exception {
        ResetPasswordRequest request = new ResetPasswordRequest("dataForTest1@gmail.com", "0123456789");

        Mockito.doThrow(new UserNotFoundException(request.login()))
                .when(authService).resetPassword(Mockito.any());

        mockMvc.perform(post("/auth/resetPassword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().is4xxClientError());
    }

}
