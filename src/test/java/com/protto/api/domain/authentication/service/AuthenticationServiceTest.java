package com.protto.api.domain.authentication.service;

import com.protto.api.data.repositories.UserRepository;
import com.protto.api.domain.authentication.model.dao.AuthenticationRequest;
import com.protto.api.domain.authentication.model.dao.AuthenticationResponse;
import com.protto.api.domain.authentication.model.dao.ResetPasswordRequest;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.domain.user.errors.exceptions.UserNotFoundException;
import com.protto.api.security.details.UserDetailsForAuthentication;
import com.protto.api.util.JWTutil;

import org.assertj.core.api.Assertions;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.test.context.jdbc.Sql;

import java.util.List;

@DataJpaTest
class AuthenticationServiceTest {
    JWTutil jwtUtil;
    @MockBean
    AuthenticationManager authenticationManager;
    @Autowired
    UserRepository userRepository;
    AuthenticationService authenticationService;
    UserDetailsForAuthentication clientDetails;
    BCryptPasswordEncoder encoder;

    @BeforeEach
    void setup() {
        encoder = new BCryptPasswordEncoder();
        jwtUtil = Mockito.mock(JWTutil.class, Mockito.RETURNS_DEEP_STUBS);
        authenticationService = new AuthenticationService(jwtUtil, authenticationManager,userRepository);
        clientDetails = new UserDetailsForAuthentication(
                "ottinestv@gmail.com", "", List.of(new SimpleGrantedAuthority(ERole.CLIENT.name())));
    }

    @Test
    @Sql("/User.sql")
    void whenLoginWithGoodCredentialsClient_then_should_generateToken() {
        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenReturn(
                new UsernamePasswordAuthenticationToken(
                        clientDetails,
                        "",
                        List.of(new SimpleGrantedAuthority(ERole.CLIENT.name()))
                )
        );
        Mockito.when(jwtUtil.generateToken(Mockito.any())).thenReturn("goodtoken");

        AuthenticationResponse resp = authenticationService.login(new AuthenticationRequest("ottinestv@gmail.com", "Tototo77!"));

        Assertions.assertThat(resp.token())
                .isEqualTo("goodtoken");
    }


    @Test
    @Sql("/User.sql")
    void whenLoginWithBadCredentialsClient_then_should_throwBadCredentialException() {
        AuthenticationRequest request = new AuthenticationRequest("ottinestv@gmail.com", "wrongpass");
        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenThrow(BadCredentialsException.class);

        Assert.assertThrows(BadCredentialsException.class,
                () -> authenticationService.login(request));
    }

    @Test
    @Sql("/User.sql")
    void whenResetPassword_then_should_ThrowError() {
        ResetPasswordRequest request = new ResetPasswordRequest("dataForNothing@gmail.com", "0123456789");

        Assertions.assertThatThrownBy(() ->
                authenticationService.resetPassword(request))
                .isInstanceOf(UserNotFoundException.class);
    }

    @Test
    @Sql("/User.sql")
    void whenResetPassword_then_should_resetPassword() {
        ResetPasswordRequest request = new ResetPasswordRequest("dataForTest1@gmail.com", "0123456789");

        authenticationService.resetPassword(request);
    }
}

