package com.protto.api.domain.report.service;

import com.protto.api.data.entities.UserEntity;
import com.protto.api.data.repositories.ReportRepository;
import com.protto.api.domain.authentication.model.enums.ERole;
import com.protto.api.util.model.ModelParser;
import com.protto.api.domain.report.errors.exceptions.ReportNoAccessException;
import com.protto.api.domain.report.errors.exceptions.ReportNotFoundException;
import com.protto.api.domain.report.model.dao.ReportCreateRequest;
import com.protto.api.domain.report.model.dao.ReportDateRequest;
import com.protto.api.domain.report.model.dao.ReportInTableVO;
import com.protto.api.domain.report.model.mapper.ReportMapper;
import com.protto.api.domain.user.service.UserService;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class ReportServiceTest {

    @Autowired
    private ReportRepository reportRepository;

    private final ReportMapper reportMapper = Mappers.getMapper(ReportMapper.class);

    private ReportService reportService;

    private UserService userService;
    private UserEntity CLIENT;
    private UserEntity ADMIN;

    @BeforeEach()
    void setup() {
        userService = Mockito.mock(UserService.class);
        reportService = new ReportService(reportRepository, reportMapper, userService);
         this.CLIENT = UserEntity.builder()
                .id(2L)
                .firstName("William")
                .lastName("Becuwe")
                .login("dataForTest2@gmail.com")
                .role(ERole.CLIENT)
                .build();

        this.ADMIN = UserEntity.builder()
                .id(1L)
                .firstName("Ottman")
                .lastName("Becuwe")
                .login("dataForTest1@gmail.com")
                .role(ERole.ADMIN)
                .build();
    }
    @Test
    @Sql("/Report.sql")
    void whenCreateReportAsCLIENT_then_should_success() {
        ReportCreateRequest reportCreateRequest = new ReportCreateRequest(
                LocalDate.now(),
                "WilliCom",
                "WillySelf",
                "WillyProject"
        );

        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        assertThat(reportService.create(reportCreateRequest, userMock)).isInstanceOf(Long.class);
    }

    @Test
    @Sql("/Report.sql")
    void whenCreateReportFromUploadRequest_then_shouldSuccess() {
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        Map<String, String> map = new java.util.HashMap<>(Map.of(
                ModelParser.EMAIL_KEY, "dataForTest2@gmail.com",
                ModelParser.REPORT_DATE_KEY, "2023-03-03",
                ModelParser.LASTNAME_KEY, "Becuwe",
                ModelParser.FIRSTNAME_KEY, "William",
                ModelParser.COM_KEY, "ComBlablabla",
                ModelParser.SELF_KEY, "SelfBlablabla",
                ModelParser.PROJECT_KEY, "ProBlablabla"));

        assertThat(reportService.create(map, userMock)).isInstanceOf(Long.class);
    }

    @Test
    @Sql("/Report.sql")
    void whenGetAllReport_then_should_returnGoodSizeList() {
        List<ReportInTableVO> reportVOList = reportService.getAll();

        assertThat(reportVOList).hasSize(2);
    }

    @Test
    void whenGetAllReport_then_should_sizeZero() {
        List<ReportInTableVO> reportVOList = reportService.getAll();

        assertThat(reportVOList).isEmpty();
    }

    @Test
    @Sql("/Report.sql")
    void whenDeleteReportAsCLIENT_then_should_deleteReport() {
        UserEntity user = UserEntity.builder()
                .id(2L)
                .firstName("William")
                .lastName("Becuwe")
                .login("dataForTest2@gmail.com")
                .role(ERole.CLIENT)
                .build();

        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(user);

        reportService.delete(1L, Mockito.any());
        assertEquals(1, reportService.getAll().size());
    }

    @Test
    @Sql("/Report.sql")
    void whenDeleteReportAsCLIENTButNoAccess_then_should_throwError() {
        UserEntity user = UserEntity.builder()
                .id(3L)
                .firstName("Alexandre")
                .lastName("Becuwe")
                .login("dataForTest3@gmail.com")
                .role(ERole.CLIENT)
                .build();

        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(user);
        Authentication userMock = Mockito.any(Authentication.class);

        Assert.assertThrows(ReportNoAccessException.class,
                () -> reportService.delete(1L, userMock));
        assertEquals(2, reportService.getAll().size());
    }

    @Test
    @Sql("/Report.sql")
    void whenDeleteReportAsCLIENTButNoAcess_then_should_throwError() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);

        Authentication userMock = Mockito.any(Authentication.class);

        Assert.assertThrows(ReportNotFoundException.class,
                () -> reportService.delete(666L, userMock));
        assertEquals(2, reportService.getAll().size());
    }

    @Test
    @Sql("/Report.sql")
    void whenDeleteReportAsCLIENT_then_should_throwError() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        Assert.assertThrows(ReportNotFoundException.class,
                () -> reportService.delete(666L, userMock));
        assertEquals(2, reportService.getAll().size());
    }

    @Test
    void whenDeleteReportAsADMIN_then_should_throwError() {

        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(true);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(ADMIN);
        Authentication userMock = Mockito.any(Authentication.class);

        Assert.assertThrows(ReportNotFoundException.class,
                () -> reportService.delete(1L, userMock));
    }

    @Test
    @Sql("/Report.sql")
    void whenDeleteReportAsADMIN_then_should_deleteReport() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(true);

        reportService.delete(1L, Mockito.any());
        assertEquals(1, reportService.getAll().size());
    }

    @Test
    @Sql("/Report.sql")
    void whenFindMyReportsAsADMIN_then_should_emptyList() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(ADMIN);
        Authentication userMock = Mockito.any(Authentication.class);

        assertThat(reportService.findMyReports(userMock)).isEmpty();
    }

    @Test
    @Sql("/Report.sql")
    void whenFindMyReports_then_should_success() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        assertThat(reportService.findMyReports(userMock)).hasSize(2);
    }

    @Test
    @Sql("/Report.sql")
    void whenFindMyReportFilterByDates_then_should_success() {
        ReportDateRequest request = new ReportDateRequest(2023, 1);

        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        assertThat(reportService.findMyReportsFilterByProvidedDate(request, userMock)).hasSize(2);
    }

    @Test
    @Sql("/Report.sql")
    void whenGetReport_then_shouldBeReportExpected() {
        assertThat(reportService.getReport(1L).name()).isEqualTo("Weekly_report_willy_20230106");
    }

    @Test
    @Sql("/Report.sql")
    void whenGetReport_then_shouldBeThrowError() {
        Assert.assertThrows(ReportNotFoundException.class,
                () -> reportService.getReport(666L));
    }

    @Test
    @Sql("/Report.sql")
    void when_findNameFromReport_then_shouldReturnGoodName() {
        assertThat(reportService.getNameFromReport(2L)).isEqualTo("Weekly_report_willy_20230113");
    }

    @Test
    @Sql("/Report.sql")
    void when_findNameFromReport_then_shouldThrowError() {
        Assert.assertThrows(ReportNotFoundException.class,
                () -> reportService.getNameFromReport(666L));
    }

    @Test
    @Sql("/Report.sql")
    void hasAccessP_asClient_shouldReturnTrue() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        Assertions.assertTrue(reportService.hasAccessP(1L, userMock));
    }

    @Test
    @Sql("/Report.sql")
    void hasAccessP_asClient_shouldReturnFalse() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(false);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(CLIENT);
        Authentication userMock = Mockito.any(Authentication.class);

        Assertions.assertFalse(reportService.hasAccessP(3L, userMock));
    }

    @Test
    @Sql("/Report.sql")
    void hasAccessP_asAdmin_shouldReturnTrue() {
        Mockito.when(userService.isAdmin(Mockito.any())).thenReturn(true);
        Mockito.when(userService.getUserByAuthentication(Mockito.any())).thenReturn(ADMIN);
        Authentication userMock = Mockito.any(Authentication.class);

        Assertions.assertTrue(reportService.hasAccessP(1L, userMock));
    }
}
