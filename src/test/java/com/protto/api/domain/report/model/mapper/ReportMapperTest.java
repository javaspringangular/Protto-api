package com.protto.api.domain.report.model.mapper;

import com.protto.api.data.entities.ReportEntity;

import com.protto.api.domain.report.model.dao.ReportInTableVO;
import com.protto.api.domain.report.model.dao.ReportVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertNull;

public class ReportMapperTest {

    private final ReportMapper reportMapper = Mappers.getMapper(ReportMapper.class);


    private final ReportEntity reportEntity = ReportEntity.builder()
            .id(1L)
            .name("Ottman")
            .reportDate(LocalDate.now())
            .depositDate(LocalDate.now())
            .userLogin("Totototo77")
            .comExchanges("Com")
            .selfStudies("Self")
            .projects("Proj")
            .build();

    @Test
    void whenMapEntityToVO_then_shouldSuccessMapping(){
        ReportVO reportVO = reportMapper.entityToReportVo(reportEntity);

        Assertions.assertEquals(reportVO.id(), reportEntity.getId());
        Assertions.assertEquals(reportVO.name(), reportEntity.getName());
        Assertions.assertEquals(reportVO.reportDate(), reportEntity.getReportDate());
        Assertions.assertEquals(reportVO.depositDate(), reportEntity.getDepositDate());
        Assertions.assertEquals(reportVO.comExchanges(), reportEntity.getComExchanges());
        Assertions.assertEquals(reportVO.selfStudies(), reportEntity.getSelfStudies());
        Assertions.assertEquals(reportVO.projects(), reportEntity.getProjects());
    }

    @Test
    void whenMapNullToVO_then_shouldBeNull(){
        ReportVO reportVO = reportMapper.entityToReportVo(null);

        assertNull(reportVO);
    }

    @Test
    void whenMapEntityToReportInTableVO_then_should_successMapping() {
        ReportInTableVO reportInTableVO = reportMapper.entityToReportInTableVO(reportEntity);

        Assertions.assertEquals(reportInTableVO.id(), reportEntity.getId());
        Assertions.assertEquals(reportInTableVO.name(), reportEntity.getName());
        Assertions.assertEquals(reportInTableVO.date(), reportEntity.getReportDate());
        Assertions.assertEquals(reportInTableVO.depositDate(), reportEntity.getDepositDate());
    }

    @Test
    void whenMapEntityToReportInTableVO_then_should_beNull() {
        ReportInTableVO reportInTableVO = reportMapper.entityToReportInTableVO(null);

        Assertions.assertNull(reportInTableVO);
    }
}
