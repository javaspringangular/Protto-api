package com.protto.api.domain.report.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.protto.api.TestUtil;

import com.protto.api.domain.report.model.dao.ReportCreateRequest;
import com.protto.api.domain.report.model.dao.ReportDateRequest;
import com.protto.api.domain.report.model.dao.ReportInTableVO;
import com.protto.api.domain.report.service.ReportService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReportController.class)
@AutoConfigureMockMvc(addFilters = false)
class ReportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReportService reportService;

    private ObjectMapper objectMapper;

    private ReportInTableVO report1;
    private ReportInTableVO report2;
    private ReportInTableVO reportDate;

    @BeforeEach
    void setup() {
        this.objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        report1 = new ReportInTableVO(
                1L,
                "report1",
                LocalDate.now(),
                LocalDate.now()
        );

        report2 = new ReportInTableVO(
                2L,
                "report2",
                LocalDate.now(),
                LocalDate.now()
        );

        reportDate = new ReportInTableVO(
                3L,
                "report3date",
                LocalDate.now(),
                LocalDate.now()
        );
    }

    @Test
    void whenGetAllReports_then_shouldSuccess() throws Exception {
        Mockito.when(reportService.getAll()).thenReturn(Arrays.asList(report1,report2));

        MvcResult mvcResult = mockMvc.perform(get("/report")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<ReportInTableVO> reportVOList = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<>() {});
        assertThat(reportVOList).contains(report1).contains(report2);
    }

    @Test
    void deleteReportWithBadParam_then_should_throwBadRequest() throws Exception {
        mockMvc.perform(delete("/report/remove/toto"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteReportWithGoodParam_then_should_delete() throws Exception {
        mockMvc.perform(delete("/report/remove/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenCreateReport_then_should_success() throws Exception {
        ReportCreateRequest reportCreateRequest = new ReportCreateRequest(
                LocalDate.now(),
                "WilliCom",
                "WillySelf",
                "WillyProject"
        );

        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();

        Mockito.when(reportService.create(reportCreateRequest, loggedUser)).thenReturn(3L);

        MvcResult result = mockMvc.perform(post("/report/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(reportCreateRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("3");
    }

    @Test
    void whenFindMyReportFilterByDate_then_shouldReturnCompleteList() throws Exception {
        ReportDateRequest request = new ReportDateRequest(
                2023,
                1
        );

        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        Mockito.when(reportService.findMyReportsFilterByProvidedDate(request, loggedUser)).thenReturn(List.of(reportDate));

        MvcResult result = mockMvc.perform(post("/report/myReports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.toJSON(request)))
                .andExpect(status().isOk())
                .andReturn();
        List<ReportInTableVO> expectedList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertThat(expectedList)
                .contains(reportDate);
    }

    @Test
    void whenFindMyReports_then_shouldReturnCompleteList() throws Exception {
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();

        Mockito.when(reportService.findMyReports(loggedUser)).thenReturn(List.of(report1,report2));

        MvcResult result = mockMvc.perform(get("/report/myReports")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<ReportInTableVO> expectedList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertThat(expectedList)
                .contains(report1)
                .contains(report2);
    }
}
