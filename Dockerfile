FROM maven:3.8.4-openjdk-17 as builder
ADD . app
WORKDIR app

RUN mvn clean package -DskipTests

FROM openjdk:17 as exploder

WORKDIR app

ARG JAR_FILE=app/target/*.jar
COPY --from=builder ${JAR_FILE} protto_api.jar

RUN java -Djarmode=layertools -jar protto_api.jar extract

FROM openjdk:17

WORKDIR app

COPY --from=exploder app/dependencies/ ./
COPY --from=exploder app/snapshot-dependencies/ ./
COPY --from=exploder app/spring-boot-loader/ ./
COPY --from=exploder app/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]